#!/usr/bin/env python2

print """
/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_hashed.hpp
 * @brief GCode hash values of string inputs
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
"""
print "#ifndef GCODE_HASH_H"
print "#define GCODE_HASH_H"
print 
print "/* Content of this tile is generate so do not edit it */"

for i in xrange(100):
    cmd = 'G' + str(i)
    off = 0
    value = 0
    for c in cmd:
        ic = ord(c) << off
        value |= ic
        off += 8

    print "#define {0} {1}".format(cmd, hex(value) )

print 

for i in xrange(1000):
    cmd = 'M' + str(i)
    off = 0
    value = 0
    for c in cmd:
        ic = ord(c) << off
        value |= ic
        off += 8

    print "#define {0} {1}".format(cmd, hex(value) )

print
print "#endif /* GCODE_HASH_H */"