#include <cstdlib>
#include <chrono>
#include <thread>
#include <iostream>
#include "serial_bridge.hpp"

using namespace test;
using namespace std::literals;

unsigned SerialBridge::ref_count = 0;

SerialBridge::SerialBridge() {
	createSerialPorts();
}

SerialBridge::~SerialBridge() {
	closeSerialPorts();
}

void SerialBridge::createSerialPorts() {
	if(!ref_count++) {
		// std::cout << "SerialBridge: START\n";
		// Make sure no other socat instance is running
		std::system("pkill socat");
		std::system("socat -d -d pty,raw,echo=0,link=/tmp/ttyDUT0 pty,raw,echo=0,link=/tmp/ttyDUT1 > /dev/null 2>&1 &");
		std::this_thread::sleep_for(1s);
	}
}

void SerialBridge::closeSerialPorts() {
	if(!--ref_count) {
		std::system("pkill socat");
		// std::cout << "SerialBridge: FINISH\n";
	}
}