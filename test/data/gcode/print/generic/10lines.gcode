M107
M115 U3.0.9 ; tell printer latest fw version
M83  ; extruder relative mode
M104 S210 ; set extruder temp
M140 S65 ; set bed temp
M190 S65 ; wait for bed temp
M109 S210 ; wait for extruder temp
G28 W ; home all without mesh bed level
G80 ; mesh bed leveling
G1 Y-3.0 F1000.0 ; go outside pritn area
