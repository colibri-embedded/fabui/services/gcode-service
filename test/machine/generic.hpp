/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: generic.hpp
 * @brief Generic Machine class definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef TEST_MACHINE_GENERIC_HPP
#define TEST_MACHINE_GENERIC_HPP

#include <string>
#include <thread>
#include <serialxx/thread_safe.hpp>
#include <fabui/thread/event.hpp>
#include <fabui/thread/queue.hpp>

namespace test {

class GenericMachine {
	public:
		/// Create new GenericMachine object
		GenericMachine(const std::string& portName,
			unsigned baudRate=115200,
			unsigned timeout=1000,
			const std::string& defaultReply="ok",
			const std::string& lineEnd="\n");
		/// Destructor
		virtual ~GenericMachine();

		/// Start threads
		virtual void start();
		/// Stop threads
		virtual void stop();

		virtual bool onReceive(const std::string& line);
		bool send(const std::string& line, bool blocking=true);
	protected:

		// unsigned tx_count;
		// unsigned rx_count;

		std::string m_default_reply;
		std::string m_line_end;

		unsigned m_timeout;
		serialxx::thread_safe::Serial m_serial;

		std::mutex m_running_mut;
		bool m_running;

		fabui::Queue<std::string, 1> txq;

		fabui::Event e_tx_state_changed;
		std::thread m_sender_thread;
		void senderThreadLoop();
		void writeLine(const std::string& line);

		fabui::Event e_rx_state_changed;
		std::thread m_receiver_thread;
		void receiverThreadLoop();
};

} // namespace test

#endif /* TEST_MACHINE_GENERIC_HPP */
