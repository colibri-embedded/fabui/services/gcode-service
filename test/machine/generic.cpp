#include "generic.hpp"
#include <iostream>
#include <fabui/utils/string.hpp>

using namespace test;

GenericMachine::GenericMachine(const std::string& portName, 
		unsigned baudRate, unsigned timeout,
		const std::string& defaultReply,
		const std::string& lineEnd) 
	: m_timeout(timeout)
	, m_default_reply(defaultReply)
	, m_line_end(lineEnd)
	, m_serial(portName, baudRate, timeout)
	, m_running(false)
{
	// tx_count = 0;
	// rx_count = 0;
}

GenericMachine::~GenericMachine()
{
	// std::cout << "TX: " << tx_count << std::endl;
	// std::cout << "RX: " << rx_count << std::endl;
	stop();
}

void GenericMachine::start() 
{
	m_serial.open();

	{
		std::lock_guard<std::mutex>  lock(m_running_mut);
		if(m_running)
			return;
		m_running = true;
	}

	m_sender_thread = std::thread(&GenericMachine::senderThreadLoop, this);
	m_receiver_thread = std::thread(&GenericMachine::receiverThreadLoop, this);

	e_rx_state_changed.wait();
	e_tx_state_changed.wait();

	e_rx_state_changed.clear();
	e_tx_state_changed.clear();
}

void GenericMachine::stop()
{
	{
		std::lock_guard<std::mutex>  lock(m_running_mut);
		if(not m_running)
			return;
		m_running = false;
	}

	send("", true);
	m_serial.abort();

	if(m_sender_thread.joinable())
		m_sender_thread.join();
	if(m_receiver_thread.joinable())
		m_receiver_thread.join();

	m_serial.close();
}

bool GenericMachine::onReceive(const std::string& line)
{
	return false;
}

void GenericMachine::receiverThreadLoop() 
{
	e_rx_state_changed.set();
	bool running = true;

	#define SYNC_RUNNING { std::lock_guard<std::mutex>  lock(m_running_mut); running = m_running; }

	std::string prev_fragment;

	while(true) {
		SYNC_RUNNING;

		if(not running) {
			break;
		}

		try {
			std::string data;
			if( m_serial.readLine(data, m_timeout) ) {

				bool save_last = false;
				if( data.c_str()[data.length()-1] != '\n' ) {
					save_last = true;
				}

				auto lines = utils::str::split(data, '\n');
				unsigned len = lines.size();
				for(unsigned i=0; i<len; i++) {
					auto &line = lines[i];
					if(save_last and (i+1) == len) {
						prev_fragment = line;
					} else {
						if(!prev_fragment.empty()) {
							prev_fragment += line;
							//processReplyLine(prev_fragment);
							// std::cout << "Machine: #1 << " << line << std::endl;
							// rx_count++;
							if(not onReceive(line) ) {
								send(m_default_reply);
								// writeLine(m_default_reply);
							}
							prev_fragment.clear();
						} else {
							// std::cout << "Machine: #2 << " << line << std::endl;
							// rx_count++;
							if(not onReceive(line) ) {
								send(m_default_reply);
								// writeLine(m_default_reply);
							}
						}
						
					}
				}
			}
			
			SYNC_RUNNING;
		} catch(...) {
			//
		}
	};

	e_rx_state_changed.set();
}

void GenericMachine::writeLine(const std::string& line)
{
	m_serial.write(line + m_line_end);
	// tx_count++;
}

bool GenericMachine::send(const std::string& line, bool blocking) 
{
	std::this_thread::sleep_for( std::chrono::microseconds(20)  );

	// std::cout << ">> " << line << std::endl;

	if(blocking) {
		txq.push(line);
		return true;
	}
	return txq.try_push(line);
}

void GenericMachine::senderThreadLoop()
{
	e_tx_state_changed.set();
	bool running = true;

	#define SYNC_RUNNING { std::lock_guard<std::mutex>  lock(m_running_mut); running = m_running; }

	while(true) {
		SYNC_RUNNING;

		if(not running) {			
			break;
		}

		std::string line;
		txq.pop(line);

		SYNC_RUNNING;

		if(not line.empty())
			writeLine(line);

	};

	e_tx_state_changed.set();
}

