#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <gcode_parser.hpp>
#include <gcode_modifier.hpp>
#include <iostream>

using namespace fabui;

SCENARIO("Z override value is update", "[z-override]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeParser parser;
		GCodeModifier gm(state);

		WHEN("Initialized") {
			THEN("Values are set to default") {
				auto o = gm.getOverride("z");

				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.0f );	// value

				std::string line, comment;
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Positive value is set") {
			THEN("Value is updated and new lines are generated") {
				gm.setOverride("z", 0.5f);
				auto o = gm.getOverride("z");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.5f );	// value

				std::string line, comment;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G91" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G0Z0.5" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G90" );

				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Negative value is set") {
			THEN("Value is updated and new lines are generated") {
				gm.setOverride("z", -0.5f);
				auto o = gm.getOverride("z");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == -0.5f );	// value

				std::string line, comment;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G91" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G0Z-0.5" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G90" );

				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("A greater value is set") {
			gm.setOverride("z", 0.5f);

			std::string line, comment;
			REQUIRE( gm.nextLine(line, comment) );
			REQUIRE( gm.nextLine(line, comment) );
			REQUIRE( gm.nextLine(line, comment) );
			REQUIRE_FALSE( gm.nextLine(line, comment) );

			THEN("Value is updated and new lines are generated") {
				gm.setOverride("z", 0.6f);
				auto o = gm.getOverride("z");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.6f );	// value

				
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G91" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G0Z0.1" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G90" );

				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("A smaller value is set") {
			gm.setOverride("z", 0.5f);

			std::string line, comment;
			REQUIRE( gm.nextLine(line, comment) );
			REQUIRE( gm.nextLine(line, comment) );
			REQUIRE( gm.nextLine(line, comment) );
			REQUIRE_FALSE( gm.nextLine(line, comment) );

			THEN("Value is updated and new lines are generated") {
				gm.setOverride("z", 0.4f);
				auto o = gm.getOverride("z");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.4f );	// value

				
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G91" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G0Z-0.1" );

				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "G90" );

				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

	}

}

SCENARIO("Z override transforms input", "[z-override]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeParser parser;
		GCodeModifier gm(state);
		
		std::string gcode = "G0 X0.5 Y0.6 Z0.7";
		std::string comment;

		WHEN("Initialized") {
			THEN("Nothing is modified") {
				auto go = parser.parse(gcode);
				REQUIRE_FALSE( gm.transform(go, gcode, comment) ); /* no skip */
				REQUIRE( gcode == "G0 X0.5 Y0.6 Z0.7" );
			}
		}

		WHEN("Positive value is set") {
			gm.setOverride("z", 0.3f);

			THEN("GCode is modified correctly") {
				auto go = parser.parse(gcode);
				REQUIRE_FALSE( gm.transform(go, gcode, comment) ); /* no skip */
				REQUIRE( gcode == "G0 X0.5 Y0.6 Z1" );
			}
		}

		WHEN("Negative value is set") {
			gm.setOverride("z", -0.3f);

			THEN("GCode is modified correctly") {
				auto go = parser.parse(gcode);
				REQUIRE_FALSE( gm.transform(go, gcode, comment) ); /* no skip */
				REQUIRE( gcode == "G0 X0.5 Y0.6 Z0.4" );
			}
		}


		WHEN("Call restore on a unlocked override") {
			gm.setOverride("z", 0.3f);
			auto o = gm.getOverride("z");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 0.3f ); // value

			gm.restore("z");

			THEN("Override is deactivated") {
				auto o = gm.getOverride("z");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.0f ); // value
			}	
		}

		WHEN("Call restore on a locked override") {
			gm.setOverride("z", 0.3f);
			gm.setLocked("z", true);
			auto o = gm.getOverride("z");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true ); // locked
			REQUIRE( std::get<2>(o) == 0.3f ); // value

			gm.restore("z");

			THEN("Override is not changed") {
				auto o = gm.getOverride("z");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == true );// locked
				REQUIRE( std::get<2>(o) == 0.3f ); // value
			}	
		}

		WHEN("Call reset on a z override") {
			gm.setOverride("z", 0.3f);
			gm.setLocked("z", true);
			auto o = gm.getOverride("z");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 0.3f );// value

			gm.reset("z");

			THEN("Override is not changed") {
				auto o = gm.getOverride("z");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.0f );// value
			}	
		}	

	}

}

SCENARIO("Speed override", "[speed-override]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeParser parser;
		GCodeModifier gm(state);

		std::string gcode = "M220 S150";
		std::string comment;

		WHEN("Initialized") {
			THEN("Values are set to default") {
				auto o = gm.getOverride("speed");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 100.0f );// value

				std::string line;
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("New value is set") {
			gm.setOverride("speed", 130.0f);
			auto o = gm.getOverride("speed");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("New lines are generated") {
				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M220 S130" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Override is not locked") {
			gm.setOverride("speed", 130.0f);
			auto o = gm.getOverride("speed");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("Gcode is not skipped and not modified") {
				auto go = parser.parse(gcode);
				REQUIRE_FALSE( gm.transform(go, gcode, comment) );
				REQUIRE( gcode == "M220 S150" );
			}	
		}

		WHEN("Override is locked") {
			gm.setOverride("speed", 130.0f);
			gm.setLocked("speed");
			auto o = gm.getOverride("speed");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("Gcode is skipped") {
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

		WHEN("Call restore on a unlocked override") {
			gm.setOverride("speed", 130.0f);
			auto o = gm.getOverride("speed");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			gm.restore("speed");

			THEN("Override is deactivated") {
				auto o = gm.getOverride("speed");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 100.0f );// value
			}	
		}

		WHEN("Call restore on a locked override") {
			gm.setOverride("speed", 130.0f);
			gm.setLocked("speed", true);
			auto o = gm.getOverride("speed");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			gm.restore("speed");

			THEN("Override is not changed") {
				auto o = gm.getOverride("speed");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == true );	// locked
				REQUIRE( std::get<2>(o) == 130.0f );// value
			}	
		}

		WHEN("Call reset on a locked override") {
			gm.setOverride("speed", 130.0f);
			gm.setLocked("speed", true);
			auto o = gm.getOverride("speed");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			gm.reset("speed");

			THEN("Override is not changed") {
				auto o = gm.getOverride("speed");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 100.0f );// value
			}	
		}		

		WHEN("Skip gcode when locked") {
			gm.setOverride("speed", 130.0f);
			gm.setLocked("speed", true);
			auto o = gm.getOverride("speed");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("Gcode is skipped") {
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

	}

}

SCENARIO("Flow override", "[flow-override]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeParser parser;
		GCodeModifier gm(state);

		std::string gcode = "M221 S150";
		std::string comment;

		WHEN("Initialized") {
			THEN("Values are set to default") {
				auto o = gm.getOverride("flow");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 100.0f );// value

				std::string line;
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("New value is set") {
			gm.setOverride("flow", 130.0f);
			auto o = gm.getOverride("flow");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("New lines are generated") {
				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M221 S130" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Override is not locked") {
			gm.setOverride("flow", 130.0f);
			auto o = gm.getOverride("flow");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("Gcode is not skipped and not modified") {
				auto go = parser.parse(gcode);
				REQUIRE_FALSE( gm.transform(go, gcode, comment) );
				REQUIRE( gcode == "M221 S150" );
			}	
		}

		WHEN("Override is locked") {
			gm.setOverride("flow", 130.0f);
			gm.setLocked("flow");
			auto o = gm.getOverride("flow");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("Gcode is skipped") {
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

		WHEN("Call restore on a unlocked override") {
			gm.setOverride("flow", 130.0f);
			auto o = gm.getOverride("flow");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			gm.restore("flow");

			THEN("Override is deactivated") {
				auto o = gm.getOverride("flow");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 100.0f );// value
			}	
		}

		WHEN("Call restore on a locked override") {
			gm.setOverride("flow", 130.0f);
			gm.setLocked("flow", true);
			auto o = gm.getOverride("flow");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			gm.restore("flow");

			THEN("Override is not changed") {
				auto o = gm.getOverride("flow");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == true );	// locked
				REQUIRE( std::get<2>(o) == 130.0f );// value
			}	
		}

		WHEN("Skip gcode when locked") {
			gm.setOverride("flow", 130.0f);
			gm.setLocked("flow", true);
			auto o = gm.getOverride("flow");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 130.0f );// value

			THEN("Gcode is skipped") {
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

	}

}

SCENARIO("Fan override", "[fan-override]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeParser parser;
		GCodeModifier gm(state);

		std::string gcode = "M106 S200";
		std::string comment;

		WHEN("Initialized") {
			THEN("Values are set to default") {
				auto o = gm.getOverride("fan");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 100.0f );// value

				std::string line;
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("New value is set") {
			gm.setOverride("fan", 50.0f);
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 50.0f );// value

			THEN("New lines are generated") {
				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M106 S127" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Zero value is set") {
			gm.setOverride("fan", 50.0f);
			gm.setOverride("fan", 0.0f);
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 0.0f );// value

			THEN("New lines are generated") {
				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M106 S127" );
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M107" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Override is not locked") {
			gm.setOverride("fan", 50.0f);
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 50.0f );// value

			THEN("Gcode is not skipped and not modified") {
				auto go = parser.parse(gcode);
				REQUIRE_FALSE( gm.transform(go, gcode, comment) );
				REQUIRE( gcode == "M106 S200" );
			}	
		}

		WHEN("Override is locked") {
			gm.setOverride("fan", 50.0f);
			gm.setLocked("fan");
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 50.0f );// value

			THEN("Gcode is skipped") {
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

		WHEN("Call restore on a unlocked override") {
			gm.setOverride("fan", 50.0f);
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 50.0f );// value

			gm.restore("fan");

			THEN("Override is deactivated") {
				auto o = gm.getOverride("fan");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 100.0f );// value
			}	
		}

		WHEN("Call restore on a locked override") {
			gm.setOverride("fan", 50.0f);
			gm.setLocked("fan", true);
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 50.0f );// value

			gm.restore("fan");

			THEN("Override is not changed") {
				auto o = gm.getOverride("fan");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == true );	// locked
				REQUIRE( std::get<2>(o) == 50.0f );// value
			}	
		}

		WHEN("Call transform when override is locked") {
			gm.setOverride("fan", 50.0f);
			gm.setLocked("fan", true);
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 50.0f );// value

			THEN("Gcode is skipped") {
				gcode = "M106 S200";
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

		WHEN("Call transform when override is locked") {
			gm.setOverride("fan", 50.0f);
			gm.setLocked("fan", true);
			auto o = gm.getOverride("fan");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 50.0f );// value

			THEN("Gcode is skipped") {
				gcode = "M107";
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

	}

}

SCENARIO("Bet_temp override", "[bed-temp-override]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeParser parser;
		GCodeModifier gm(state);

		std::string gcode = "M140 S250";
		std::string comment;

		WHEN("Initialized") {
			THEN("Values are set to default") {
				auto o = gm.getOverride("bed_temp");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.0f );// value

				std::string line;
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("New value is set") {
			gm.setOverride("bed_temp", 180.0f);
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			THEN("New lines are generated") {
				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M140 S180" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Zero value is set") {
			gm.setOverride("bed_temp", 0.0f);
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 0.0f );// value

			THEN("New lines are generated") {
				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M140 S0" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Override is not locked") {
			gm.setOverride("bed_temp", 180.0f);
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			THEN("Gcode is not skipped and not modified") {
				auto go = parser.parse(gcode);
				REQUIRE_FALSE( gm.transform(go, gcode, comment) );
				REQUIRE( gcode == "M140 S250" );
			}	
		}

		WHEN("Override is locked") {
			gm.setOverride("bed_temp", 180.0f);
			gm.setLocked("bed_temp");
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			THEN("Gcode is skipped") {
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

		WHEN("Call restore on a unlocked override") {
			gm.setOverride("bed_temp", 180.0f);
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == false );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			gm.restore("bed_temp");

			THEN("Override is deactivated") {
				auto o = gm.getOverride("bed_temp");
				
				REQUIRE( std::get<0>(o) == false ); // active
				REQUIRE( std::get<1>(o) == false );	// locked
				REQUIRE( std::get<2>(o) == 0.0f );// value
			}	
		}

		WHEN("Call restore on a locked override") {
			gm.setOverride("bed_temp", 180.0f);
			gm.setLocked("bed_temp", true);
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			gm.restore("bed_temp");

			THEN("Override is not changed") {
				auto o = gm.getOverride("bed_temp");
				
				REQUIRE( std::get<0>(o) == true ); // active
				REQUIRE( std::get<1>(o) == true );	// locked
				REQUIRE( std::get<2>(o) == 180.0f );// value
			}	
		}

		WHEN("Call transform when override is locked") {
			gm.setOverride("bed_temp", 180.0f);
			gm.setLocked("bed_temp", true);
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			THEN("Gcode is skipped") {
				gcode = "M190 S200";
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

		WHEN("Call transform when override is locked") {
			gm.setOverride("bed_temp", 180.0f);
			gm.setLocked("bed_temp", true);
			auto o = gm.getOverride("bed_temp");
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			THEN("Gcode is skipped") {
				gcode = "M140 S200";
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

	}

}

SCENARIO("Nozzle_temp override", "[bed-temp-override]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeParser parser;
		GCodeModifier gm(state);

		std::string gcode = "M104 S250";
		std::string comment;

		WHEN("Initialized") {
			THEN("Values are set to default") {
				for(unsigned i=0; i<5; i++) {
					auto o = gm.getOverride("nozzle_temp", i);
					
					REQUIRE( std::get<0>(o) == false);	// active
					REQUIRE( std::get<1>(o) == false);	// locked
					REQUIRE( std::get<2>(o) == 0.0f );	// value

					std::string line;
					REQUIRE_FALSE( gm.nextLine(line, comment) );
				}
			}
		}


		WHEN("Set new value for #0") {
			gm.setOverride("nozzle_temp", 125.0f, 0);

			THEN("Values are set to default") {
				auto o = gm.getOverride("nozzle_temp", 0);
				
				REQUIRE( std::get<0>(o) == true ); 		// active
				REQUIRE( std::get<1>(o) == false );		// locked
				REQUIRE( std::get<2>(o) == 125.0f );	// value

				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M104 S125 T0" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Set new value for #1") {
			gm.setOverride("nozzle_temp", 125.0f, 1);

			THEN("Values are set to default") {
				auto o = gm.getOverride("nozzle_temp", 1);
				
				REQUIRE( std::get<0>(o) == true ); 		// active
				REQUIRE( std::get<1>(o) == false );		// locked
				REQUIRE( std::get<2>(o) == 125.0f );	// value

				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M104 S125 T1" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Set new value for #2") {
			gm.setOverride("nozzle_temp", 125.0f, 2);

			THEN("Values are set to default") {
				auto o = gm.getOverride("nozzle_temp", 2);
				
				REQUIRE( std::get<0>(o) == true ); 		// active
				REQUIRE( std::get<1>(o) == false );		// locked
				REQUIRE( std::get<2>(o) == 125.0f );	// value

				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M104 S125 T2" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Set new value for #3") {
			gm.setOverride("nozzle_temp", 125.0f, 3);

			THEN("Values are set to default") {
				auto o = gm.getOverride("nozzle_temp", 3);
				
				REQUIRE( std::get<0>(o) == true ); 		// active
				REQUIRE( std::get<1>(o) == false );		// locked
				REQUIRE( std::get<2>(o) == 125.0f );	// value

				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M104 S125 T3" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Set new value for #4") {
			gm.setOverride("nozzle_temp", 125.0f, 4);

			THEN("Values are set to default") {
				auto o = gm.getOverride("nozzle_temp", 4);
				
				REQUIRE( std::get<0>(o) == true ); 		// active
				REQUIRE( std::get<1>(o) == false );		// locked
				REQUIRE( std::get<2>(o) == 125.0f );	// value

				std::string line;
				REQUIRE( gm.nextLine(line, comment) );
				REQUIRE( line == "M104 S125 T4" );
				REQUIRE_FALSE( gm.nextLine(line, comment) );
			}
		}

		WHEN("Call transform when override is locked") {
			gm.setOverride("nozzle_temp", 180.0f, 1);
			gm.setLocked("nozzle_temp", true, 1);
			auto o = gm.getOverride("nozzle_temp", 1);
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			THEN("Gcode is skipped") {
				gcode = "M109 S200";
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}

		WHEN("Call transform when override is locked") {
			gm.setOverride("nozzle_temp", 180.0f, 1);
			gm.setLocked("nozzle_temp", true, 1);
			auto o = gm.getOverride("nozzle_temp", 1);
			
			REQUIRE( std::get<0>(o) == true ); // active
			REQUIRE( std::get<1>(o) == true );	// locked
			REQUIRE( std::get<2>(o) == 180.0f );// value

			THEN("Gcode is skipped") {
				gcode = "M104 S200";
				auto go = parser.parse(gcode);
				REQUIRE( gm.transform(go, gcode, comment) );
			}	
		}		

	}

}

SCENARIO("Modifier json info", "[info-json]") {

	GIVEN("New GCodeModifier object") {
		GCodeState state;
		GCodeModifier gm(state);

		// std::string gcode = "M104 S250";
		// std::string comment;

		WHEN("info is called") {
			// auto info = gm.to_json();

			THEN("Json object containing current info is refurned") {
				// REQUIRE( info[""].as_string() == " " );
			}
		}

	}

}