#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <gcode_state.hpp>

using namespace fabui;

SCENARIO("GCode state object creationg", "[gcode-state]") {

	GIVEN("New state object") {
		GCodeState   s;

		WHEN("Initialized") {

			THEN("Default values are set") {
				REQUIRE( s.speed == 100.0f );
				REQUIRE( s.flow == 100.0f );
				REQUIRE( s.xyz_mode == gcode::Positioning::ABSOLUTE );
				REQUIRE( s.e_mode == gcode::Positioning::ABSOLUTE );

				REQUIRE( s.feedrate == 0 );

				REQUIRE( s.fan_pwm == 0 );

				REQUIRE( s.z == 0.0f );

				REQUIRE( s.rpm == 0 );
				REQUIRE( s.rotation == gcode::Rotation::CW );

				REQUIRE( s.bed_target == 0.0f );
				REQUIRE( s.nozzle_target[0] == 0.0f );
				REQUIRE( s.nozzle_target[1] == 0.0f );
				REQUIRE( s.nozzle_target[2] == 0.0f );
				REQUIRE( s.nozzle_target[3] == 0.0f );
				REQUIRE( s.nozzle_target[4] == 0.0f );
			}
		}
	}

	GIVEN("Modified state object") {
		GCodeState   s;
		s.speed = 110.0f;
		s.flow = 101.0f;
		s.xyz_mode = s.e_mode = gcode::Positioning::RELATIVE;
		s.feedrate = 100;
		s.fan_pwm = 255;
		s.z = 100.0f;
		s.rpm = 15000;
		s.rotation = gcode::Rotation::CCW;
		s.bed_target = 80;
		s.nozzle_target[0] = 200.0f;
		s.nozzle_target[1] = 200.0f;
		s.nozzle_target[2] = 200.0f;
		s.nozzle_target[3] = 200.0f;
		s.nozzle_target[4] = 200.0f;

		WHEN("Reset is called") {

			s.reset();
			
			THEN("Values are set to defaults") {
				REQUIRE( s.speed == 100.0f );
				REQUIRE( s.flow == 100.0f );
				REQUIRE( s.xyz_mode == gcode::Positioning::ABSOLUTE );
				REQUIRE( s.e_mode == gcode::Positioning::ABSOLUTE );

				REQUIRE( s.feedrate == 0 );

				REQUIRE( s.fan_pwm == 0 );

				REQUIRE( s.z == 0.0f );

				REQUIRE( s.rpm == 0 );
				REQUIRE( s.rotation == gcode::Rotation::CW );

				REQUIRE( s.bed_target == 0.0f );
				REQUIRE( s.nozzle_target[0] == 0.0f );
				REQUIRE( s.nozzle_target[1] == 0.0f );
				REQUIRE( s.nozzle_target[2] == 0.0f );
				REQUIRE( s.nozzle_target[3] == 0.0f );
				REQUIRE( s.nozzle_target[4] == 0.0f );
			}
		}

	}

}