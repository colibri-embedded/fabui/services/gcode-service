#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include "gcode_parser.hpp"
#include "gcode_hashed.hpp"
#include <iostream>

using namespace fabui;

// int main(int argc, char** argv) {

// 	fabui::GCodeParser gp;

// 	auto g0 = gp.parse("G0 X10.5 Y0.5 Z50 F2500");
// 	auto g1 = gp.parse("G0X10.5Y0.5Z50F2500");
// 	auto g2 = gp.parse("M220S100");

// 	std::cout << "[g0] == 'G0' : " 	<< (g0 == "G0") << std::endl;
// 	std::cout << "  X<int> = " 		<< g0.getInt('X') << std::endl;
// 	std::cout << "  X<float> = " 	<< g0.getFloat('X') << std::endl;
// 	std::cout << "  X<bool> = " 	<< g0.getBool('X') << std::endl;

// 	std::cout << "\n[g1] == 'G0' : " << (g1 == "G0") << std::endl;

// 	std::cout << "\n[g2] == 'M220' : " << (g2 == "M220") << std::endl;
// 	std::cout << "  S<int> = " 		<< g2.getInt('S') << std::endl;
// 	std::cout << "  S<float> = " 	<< g2.getFloat('S') << std::endl;
// 	std::cout << "  S<bool> = " 	<< g2.getBool('S') << std::endl;

// 	std::cout << "\n[g2] == 'M221' : " << (g2 == "M221") << std::endl;

// 	// s = "G0X10.5Y0.5Z50F2500";
// 	// std::regex z ("Z[0-9.]+");
// 	// std::cout << std::regex_replace (s, z,"Z0.0");
	
// 	return 0;
// }

SCENARIO("GCode is parsed correctly", "[gcode-parser]") {

	GIVEN("New GCodeParser object") {
		GCodeParser gp;

		WHEN("Gcode[1] without arguments is passed") {
			auto g = gp.parse("G0");

			THEN("Command is parsed and equal to hash") {
				REQUIRE(g == G0);
			}

			THEN("Command is parsed and equal to string") {
				REQUIRE(g == "G0");
			}
		}

		WHEN("Gcode[2] without arguments is passed") {
			auto g = gp.parse("G90");

			THEN("Command is parsed and equal to hash") {
				REQUIRE(g == G90);
			}

			THEN("Command is parsed and equal to string") {
				REQUIRE(g == "G90");
			}
		}

		WHEN("Gcode[3] without arguments is passed") {
			auto g = gp.parse("M999");

			THEN("Command is parsed and equal to hash") {
				REQUIRE(g == M999);
			}

			THEN("Command is parsed and equal to string") {
				REQUIRE(g == "M999");
			}
		}

		WHEN("Gcode with positive decimal argument is passed") {
			auto g = gp.parse("G0 X0.1");
			REQUIRE(g == G0);

			THEN("Float value is parsed") {
				REQUIRE( g.have('X') );
				REQUIRE_NOTHROW( g.getFloat('X') );
				REQUIRE( g.getFloat('X') == 0.1f );
			}
		}

		WHEN("Gcode with positive [+] decimal argument is passed") {
			auto g = gp.parse("G0 X+0.1");
			REQUIRE(g == G0);

			THEN("Float value is parsed") {
				REQUIRE( g.have('X') );
				REQUIRE_NOTHROW( g.getFloat('X') );
				REQUIRE( g.getFloat('X') == 0.1f );
			}
		}

		WHEN("Gcode with negative decimal argument is passed") {
			auto g = gp.parse("G0 X-0.1");
			REQUIRE(g == G0);

			THEN("Float value is parsed") {
				REQUIRE( g.have('X') );
				REQUIRE_NOTHROW( g.getFloat('X') );
				REQUIRE( g.getFloat('X') == -0.1f );
			}
		}

		WHEN("Gcode with positive int argument is passed") {
			auto g = gp.parse("G0 X1");
			REQUIRE(g == G0);

			THEN("Int value is parsed") {
				REQUIRE( g.have('X') );
				REQUIRE_NOTHROW( g.getInt('X') );
				REQUIRE( g.getInt('X') == 1 );
			}
		}

		WHEN("Gcode with positive [+] int argument is passed") {
			auto g = gp.parse("G0 X+1");
			REQUIRE(g == G0);

			THEN("Int value is parsed") {
				REQUIRE( g.have('X') );
				REQUIRE_NOTHROW( g.getInt('X') );
				REQUIRE( g.getInt('X') == 1 );
			}
		}

		WHEN("Gcode with negative int argument is passed") {
			auto g = gp.parse("G0 X-1");
			REQUIRE(g == G0);

			THEN("Int value is parsed") {
				REQUIRE( g.have('X') );
				REQUIRE_NOTHROW( g.getInt('X') );
				REQUIRE( g.getInt('X') == -1 );
			}
		}

		WHEN("Non-zero int argument is passed") {
			auto g = gp.parse("M220 S100");
			REQUIRE(g == M220);

			THEN("True bool value is parsed") {
				REQUIRE( g.have('S') );
				REQUIRE_NOTHROW( g.getBool('S') );
				REQUIRE( g.getBool('S') );
			}
		}

		WHEN("Non-zero decimal argument is passed") {
			auto g = gp.parse("M220 S100.1");
			REQUIRE(g == M220);

			THEN("True bool value is parsed") {
				REQUIRE( g.have('S') );
				REQUIRE_NOTHROW( g.getBool('S') );
				REQUIRE( g.getBool('S') );
			}
		}

		WHEN("Zero int argument is passed") {
			auto g = gp.parse("M220 S0");
			REQUIRE(g == M220);

			THEN("True bool value is parsed") {
				REQUIRE( g.have('S') );
				REQUIRE_NOTHROW( g.getBool('S') );
				REQUIRE_FALSE( g.getBool('S') );
			}
		}

		WHEN("Gcode with spaces is passed") {
			auto g = gp.parse("G1 X0.5  Y20 Z0.01  F500");

			THEN("All agruments are parsed") {
				REQUIRE( g == G1 );

				REQUIRE( g.have('X') );
				REQUIRE( g.getFloat('X') == 0.5f );

				REQUIRE( g.have('Y') );
				REQUIRE( g.getFloat('Y') == 20.0f );

				REQUIRE( g.have('Z') );
				REQUIRE( g.getFloat('Z') == 0.01f );

				REQUIRE( g.have('F') );
				REQUIRE( g.getInt('F') == 500 );
			}
		}

		WHEN("Compact gcode is passed") {
			auto g = gp.parse("G1X0.5Y20Z0.01F500");

			THEN("All agruments are parsed") {
				REQUIRE( g == G1 );

				REQUIRE( g.have('X') );
				REQUIRE( g.getFloat('X') == 0.5f );

				REQUIRE( g.have('Y') );
				REQUIRE( g.getFloat('Y') == 20.0f );

				REQUIRE( g.have('Z') );
				REQUIRE( g.getFloat('Z') == 0.01f );

				REQUIRE( g.have('F') );
				REQUIRE( g.getInt('F') == 500 );
			}
		}

	}

}

SCENARIO("GCode parser exceptions", "[gcode-parser]") {

	GIVEN("New GCodeParser object") {
		GCodeParser gp;

		WHEN("Empty string is passed") {
			THEN("An exception is thrown") {
				REQUIRE_THROWS( gp.parse("G") );
			}
		}

		WHEN("Too short string is passed") {
			THEN("An exception is thrown") {
				REQUIRE_THROWS( gp.parse("G") );
			}
		}

		WHEN("Bad format[1] string is passed") {
			THEN("An exception is thrown") {
				REQUIRE_THROWS( gp.parse("GX") );
			}
		}

		WHEN("Bad format[2] string is passed") {
			THEN("An exception is thrown") {
				REQUIRE_THROWS( gp.parse("GX0Y0Z") );
			}
		}

		WHEN("Bad format[3] string is passed") {
			THEN("An exception is thrown") {
				REQUIRE_THROWS( gp.parse("MS200") );
			}
		}

		WHEN("Bad format[4] string is passed") {
			THEN("An exception is thrown") {
				REQUIRE_THROWS( gp.parse("X0Y0Z") );
			}
		}


	}
}

SCENARIO("GCode to_string", "[gcode-parser][gcode]") {

	GIVEN("New GCodeParser object") {
		GCodeParser gp;

		WHEN("No arguments[1]") {
			auto g = gp.parse("G0");

			REQUIRE( g == G0 );

			THEN("Conversion is correct") {
				REQUIRE( g.to_string() == "G0" );
			}
		}

		WHEN("No arguments[2]") {
			auto g = gp.parse("M83");

			REQUIRE( g == M83 );

			THEN("Conversion is correct") {
				REQUIRE( g.to_string() == "M83" );
			}
		}

		WHEN("No arguments[3]") {
			auto g = gp.parse("M114");

			REQUIRE( g == M114 );

			THEN("Conversion is correct") {
				REQUIRE( g.to_string() == "M114" );
			}
		}

		WHEN("Two arguments") {
			auto g = gp.parse("G0 X1 Y2");

			REQUIRE( g == G0 );

			THEN("Conversion is correct") {
				REQUIRE( g.to_string() == "G0 X1 Y2" );
				REQUIRE( g.to_string(true) == "G0X1Y2" );
			}
		}

		WHEN("Three arguments") {
			auto g = gp.parse("G0 X1 Y2 Z3");

			REQUIRE( g == G0 );

			THEN("Conversion is correct") {
				REQUIRE( g.to_string() == "G0 X1 Y2 Z3" );
				REQUIRE( g.to_string(true) == "G0X1Y2Z3" );
			}
		}

		WHEN("Four arguments") {
			auto g = gp.parse("G0 X1 Y2 Z3 F500");

			REQUIRE( g == G0 );

			THEN("Conversion is correct") {
				REQUIRE( g.to_string() == "G0 F500 X1 Y2 Z3" );
				REQUIRE( g.to_string(true) == "G0F500X1Y2Z3" );
			}
		}

		WHEN("Five arguments") {
			auto g = gp.parse("G0 X1 Y2 Z3 E-3.4 F500");

			REQUIRE( g == G0 );

			THEN("Conversion is correct") {
				REQUIRE( g.to_string() == "G0 E-3.4 F500 X1 Y2 Z3" );
				REQUIRE( g.to_string(true) == "G0E-3.4F500X1Y2Z3" );
			}
		}

	}	

}

SCENARIO("GCode arguments modification", "[gcode-parser][gcode]") {

	GIVEN("New GCodeParser object") {
		GCodeParser gp;

		WHEN("Int argument is updated") {
			auto g = gp.parse("G0 X5.0");

			REQUIRE( g == G0 );
			REQUIRE( g.getInt('X') == 5 );
			REQUIRE_NOTHROW( g.setInt('X', 6) );

			THEN("New value is stored correctly") {
				REQUIRE( g.getInt('X') == 6 );
			}
		}

		WHEN("Float argument is updated") {
			auto g = gp.parse("G0 X5.1");

			REQUIRE( g == G0 );
			REQUIRE( g.getFloat('X') == 5.1f );
			REQUIRE_NOTHROW( g.setFloat('X', 6.2f) );

			THEN("New value is stored correctly") {
				REQUIRE( g.getFloat('X') == 6.2f );
			}
		}

		WHEN("Bool[1] argument is updated") {
			auto g = gp.parse("M302 S0");

			REQUIRE( g == M302 );
			REQUIRE_FALSE( g.getBool('S') );
			REQUIRE_NOTHROW( g.setBool('S', true) );

			THEN("New value is stored correctly") {
				REQUIRE( g.getBool('S') );
			}
		}

		WHEN("Bool[2] argument is updated") {
			auto g = gp.parse("M302 S1");

			REQUIRE( g == M302 );
			REQUIRE( g.getBool('S') );
			REQUIRE_NOTHROW( g.setBool('S', false) );

			THEN("New value is stored correctly") {
				REQUIRE_FALSE( g.getBool('S') );
			}
		}

		WHEN("One float argument is updated among several") {
			auto g = gp.parse("G1 X5.6 Y-4.56 Z0.88");

			REQUIRE( g == G1 );
			REQUIRE_NOTHROW( g.setFloat('Z', 1.2f) );

			THEN("New value is stored correctly") {
				REQUIRE( g.to_string() == "G1 X5.6 Y-4.56 Z1.2" );
				REQUIRE( g.to_string(true) == "G1X5.6Y-4.56Z1.2" );
			}
		}


	}

}
