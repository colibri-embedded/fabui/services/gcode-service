#include <plugins/gcode/marlin_1_1_x.hpp>

using namespace wampcc;
using namespace fabui;
using namespace plugin::gcode;

extern "C" Marlin_1_1_X* create(WampSession *session)
{
	return new Marlin_1_1_X(session);
}

Marlin_1_1_X::Marlin_1_1_X(WampSession *session)
	: Plugin(session)
	, m_m190_regex("T:([0-9.]+) E:(\\d+) B:([0-9.]+)")
	, m_m109_regex("T:([0-9.]+) E:(\\d+) W:\\?")
	, m_m105_regex("ok T:([0-9.]+) /0\\.0 B:([0-9.]+) /0\\.0 T0:([0-9.]+) /0\\.0 @:0 B@:0")
	, m_m303_regex("")
	, m_m114_regex("")
{
	
	
	
}

std::string Marlin_1_1_X::name()
{
	return "marlin-1.1.x";
}

std::string Marlin_1_1_X::description()
{
	return "Generic Marlin 1.1.x gcode handler";
}

// Download
// avrdude -P /dev/ttyACM0 -c wiring -p m2560 -F -U flash:r:prusa3d_fw_3.0.9.hex:i

// Upload
// avrdude -P /dev/ttyACM0 -c wiring -p m2560 -V -D -U flash:w:/mnt/projects/Prusa3D/MK2/1_75mm_MK2-RAMBo13a-E3Dv6full.hex:i


/*std::map<std::string, std::string>  Marlin_1_1_X::parseM503(service::command::GCode& command)
{
	std::map<std::string, std::string> data;
	return data;
}*/

void Marlin_1_1_X::parseAsyncLine(const std::string&)
{

}

json_object Marlin_1_1_X::parseReplyLine(command::GCode* command, const std::string& line)
{
	auto gcode = command->getData();
	json_object extra = {};

	if(gcode.compare(0, 4, "M190") == 0) {
		// M190 T:29.47 E:0 B:28.7
		std::smatch match;
		if( std::regex_match(line, match, m_m190_regex) ) {
			// T:([0-9.]+) E:([0-9]+) B:([0-9.]+)
			if(match.size() == 4) {
				auto T = std::stof(match[1]);
				auto E = std::stoi(match[2]);
				auto B = std::stof(match[3]);
				extra = {
					{"T", T},
					{"E", E},
					{"B", B}
				};

				if(m_session) {
					m_session->publish("gcode.events", { {"temp_update"}, extra });
				}
			}
		}
	} else if (gcode.compare(0, 4, "M109") == 0) {
		// M109 T:217.2 E:0 W:?
		std::smatch match;
		if( std::regex_match(line, match, m_m109_regex) ) {
			// "T:([0-9.]+) E:([0-9]+) W:\?"
			if(match.size() == 3) {
				auto T = std::stof(match[1]);
				auto E = std::stoi(match[2]);
				extra = {
					{"T", T},
					{"E", E}
				};

				if(m_session) {
					m_session->publish("gcode.events", { {"temp_update"}, extra });
				}
			}
		}
	} else if (gcode.compare(0, 4, "M105") == 0) {
		// M105 ok T:31.4 /0.0 B:30.7 /0.0 T0:31.4 /0.0 @:0 B@:0
		std::smatch match;
		if( std::regex_match(line, match, m_m105_regex) ) {
			// "ok T:([0-9.]+) /0.0 B:([0-9.]+) /0.0 T0:([0-9.]+) /0.0 @:0 B@:0"
			if(match.size() == 4) {
				auto T = std::stof(match[1]);
				auto B = std::stof(match[2]);
				auto T0 = std::stoi(match[3]);
				extra = {
					{"T", T},
					{"B", B},
					{"T0", T0}
				};

				if(m_session) {
					m_session->publish("gcode.events", { {"temp_update"}, extra });
				}
			}
		}
	} else if (gcode.compare(0, 4, "M303")) {
		// M303 
	} else if (gcode.compare(0, 4, "M114")) {
		// M114
	}

	return std::move(extra);
}