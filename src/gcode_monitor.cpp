/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_monitor.cpp
 * @brief GCode monitor implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "gcode_monitor.hpp"
#include "gcode_modifier.hpp"
#include "gcode_hashed.hpp"

using namespace fabui;

GCodeMonitor::GCodeMonitor(GCodeState &state, GCodeModifier &modifier) 
	: m_state(state)
	, m_modifier(modifier)
{
	
}

void GCodeMonitor::process(GCode& go, const std::string& /*comment*/, bool restore)
{
	switch(go.hash) {
		case G0:	// travel move, extract z height
		case G1:	// tool move, extract feedrate,
		case G2:    // CW arc
		case G3:    // CCW arc
			if(go.have('Z')) {
				m_state.z = go.getFloat('Z');
			}
			if(go.have('F')) {
				m_state.feedrate = go.getInt('F');
			}
			break;
		case G90:	// xyz absolute mode
			m_state.xyz_mode = gcode::Positioning::ABSOLUTE;
			break;
		case G91:	// xyz relative mode
			m_state.xyz_mode = gcode::Positioning::RELATIVE;
			break;
		case M82:	// e absolute mode
			m_state.e_mode = gcode::Positioning::ABSOLUTE;
			break;
		case M83:	// e relative mode
			m_state.e_mode = gcode::Positioning::RELATIVE;
			break;
		case M3:	// spindle CW / laser ON
			if(go.have('S')) {
				m_state.rpm = go.getInt('S');
				m_state.rotation = gcode::Rotation::CW;
				if(restore)
					m_modifier.restore("rpm");
			}
			break;
		case M4:	// spindle CCW / laser ON
			if(go.have('S')) {
				m_state.rpm = go.getInt('S');
				m_state.rotation = gcode::Rotation::CCW;
				if(restore)
					m_modifier.restore("rpm");
			}
			break;
		case M5:	// spindle STOP / laser OFF
			m_state.rpm = 0;
			if(restore)
				m_modifier.restore("rpm");
			break;
		case M117:	// display message
			// TODO: extract message
			break;
		case M140:	// set nozzle temp
		case M104:	// set nozzle temp and wait
			if(go.have('S')) {
				unsigned T = 0;
				if(go.have('T')) {
					T = go.getInt('T');
					if(T >= MAX_NOZZLE_COUNT)
						T = 0;
				}
				m_state.nozzle_target[T] = go.getFloat('S');
				if(restore)
					m_modifier.restore("nozzle_temp", T);
			}
			break;
		case M190:	// set bed temp
		case M109:	// set bed temp and wait
			if(go.have('S')) {
				m_state.bed_target = go.getFloat('S');
				if(restore)
					m_modifier.restore("bed_temp");
			}
			break;
		case M106:	// set fan PWM
			if(go.have('S')) {
				m_state.fan_pwm = go.getInt('S');
				if(restore)
					m_modifier.restore("fan");
			}
			break;
		case M107:	// fan OFF
			m_state.fan_pwm = 0;
			if(restore)
				m_modifier.restore("fan");
			break;
		case M220:	// set speed factor
			if(go.have('S')) {
				m_state.speed = go.getFloat('S');
				if(restore)
					m_modifier.restore("speed");
			}
			break;
		case M221:	// set flow factor
			if(go.have('S')) {
				m_state.flow = go.getFloat('S');
				if(restore)
					m_modifier.restore("flow");
			}
			break;
		// case M240:	// trigger camera
		// 	break;
		case M401:	// deploy probe
			// TODO: store probe deplyed
			break;
		case M402: // stow probe
			// TODO: store probe stowed
			break;

	}
}