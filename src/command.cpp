/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: command.cpp
 * @brief GCodeService command implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "command.hpp"

#include <iostream>
using namespace fabui::command;

Generic::Generic(Type type, 
		const std::string& command,
		const std::string &expected_reply,
		bool use_regex,
		unsigned timeout,
		bool async)
	: m_type(type)
	, m_dont_modify(false)
	, m_async(async)
	, m_id(0)
	, m_command(command)
	, m_expected_reply(expected_reply)
	, m_regex( expected_reply )
	, m_use_regex(use_regex)
{
	m_expires_at = std::chrono::system_clock::now() + std::chrono::milliseconds(timeout);
}

void Generic::setDontModify(bool value) {
	m_dont_modify = value;
}

bool Generic::dontModify() {
	return m_dont_modify;
}

bool Generic::isAsync()
{
	return m_async;
}

bool Generic::isAborted()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return (m_state == State::ABORTED);
}

bool Generic::isExpired()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return (m_state == State::EXPIRED);
}

bool Generic::checkExpired()
{
	auto now = std::chrono::system_clock::now();
	if(now > m_expires_at) {
		std::lock_guard<std::mutex> lock(m_mutex);
		m_state = State::EXPIRED;
		done.set();
		return true;
	}
	return false;
}

bool Generic::isValid()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return (m_state == State::FINISHED);
}

Generic::operator bool()
{
	return isValid();
}

void Generic::setId(unsigned id)
{
	m_id = id;
}

unsigned Generic::getId()
{
	return m_id;
}

Type Generic::getType()
{
	return m_type;
}

void Generic::finish()
{
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		m_state = State::FINISHED;
	}
	// std::cout << "cmd: finished\n";
	done.set();
}

void Generic::abort()
{
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		// Only allow abort on a command that is waiting for it's result
		if(m_state != State::WAITING)
			return;
		m_state = State::ABORTED;
	}
	std::cout << "cmd: aborted(" << m_command << ")\n";
	done.set();
}

bool Generic::wait()
{
	bool status = done.wait_until(m_expires_at);
	if(not status)
		m_state = State::EXPIRED;
	return status;
}

std::string Generic::getData()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_command;
}

std::vector<std::string> Generic::getReply()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	return m_reply;
}

bool Generic::addReplyLine(const std::string& line)
{
	bool result = false;
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		m_reply.push_back(line);
	}
	
	if(m_use_regex) {
		std::smatch m;
		result = std::regex_match(line, m, m_regex);
		// std::cout << "|" << line << "| == /" << m_expected_reply << "/ : " << result << " (" << m_command << ")" << "\n";
	} else {
		result = (line == m_expected_reply);
		// std::cout << "|" << line << "| == |" << m_expected_reply << "| : " << result << " (" << m_command << ")" << "\n";
	}

	return result;
}


wampcc::json_array Generic::toArray()
{
	wampcc::json_array ja;
	return ja;
}

wampcc::json_object Generic::toObject()
{
	wampcc::json_object jo;
	wampcc::json_array ja;
	for(auto &line: m_reply) {
		ja.push_back(line);
	}
	jo["reply"] = ja;

	std::string status = "ok";
	if(isExpired())
		status = "expired";
	else if(isAborted())
		status = "aborted";

	jo["status"] = status;
	jo["gcode"] = m_command;

	return jo;
}

/* specializations */

None::None(): Generic(Type::NONE, "") { }
Terminate::Terminate(): Generic(Type::TERMINATE, "") { }

GCode::GCode(const std::string& gcode,
			const std::string &expected_reply,
			bool use_regex,
			unsigned timeout)
	: Generic(Type::GCODE, gcode, expected_reply, use_regex, timeout)
{

}

GCodeAsync::GCodeAsync(
	const std::string& gcode,
	const std::string &expected_reply,
	bool use_regex,
	unsigned timeout)
	: Generic(Type::GCODE, gcode, expected_reply, use_regex, timeout, true)
{

}

FileLoad::FileLoad(const std::string& fileName, unsigned timeout)
	: Generic(Type::FILE_LOAD, fileName, "", false, timeout)
{ 

}

std::string FileLoad::getFileName()
{
	return m_command;
}

FilePush::FilePush(unsigned timeout)
	: Generic(Type::FILE_PUSH, "", "", false, timeout)
{ 

}

FilePause::FilePause(unsigned timeout)
	: Generic(Type::FILE_PAUSE, "", "", false, timeout)
{

}

FileResume::FileResume(unsigned timeout)
	: Generic(Type::FILE_RESUME, "", "", false, timeout)
{

}

FileAbort::FileAbort(unsigned timeout)
	: Generic(Type::FILE_ABORT, "", "", false, timeout)
{

}
