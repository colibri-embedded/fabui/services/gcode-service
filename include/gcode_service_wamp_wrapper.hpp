/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_service_wamp_wrapper.hpp
 * @brief GCodeService wamp wrapper definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODE_SERVICE_WAMP_WRAPPER_HPP
#define GCODE_SERVICE_WAMP_WRAPPER_HPP

#include "gcode_service.hpp"
#include <fabui/wamp/backend_service.hpp>

namespace fabui {

class GCodeServiceWrapper: public BackendService {
	public:
		GCodeServiceWrapper(const std::string& env_file, GCodeService& service);

	protected:
		GCodeService& m_service;

		/* backend-service */
		void onConnect();
		void onDisconnect();
		void onJoin();

		/* Service control */
		void terminate_wrapper(wampcc::invocation_info info);
		void suspend_wrapper(wampcc::invocation_info info);
		void resume_wrapper(wampcc::invocation_info info);
		void info_wrapper(wampcc::invocation_info info);

		/* Gcode functions */
		void send_wrapper(wampcc::invocation_info info);
		void sendAsync_wrapper(wampcc::invocation_info info);
		void abortAsync_wrapper(wampcc::invocation_info info);

		/* GCode File functions */
		void fileLoad_wrapper(wampcc::invocation_info info);
		void filePush_wrapper(wampcc::invocation_info info);
		void fileLoadAndPush_wrapper(wampcc::invocation_info info);
		void filePause_wrapper(wampcc::invocation_info info);
		void fileResume_wrapper(wampcc::invocation_info info);
		void fileAbort_wrapper(wampcc::invocation_info info);
		void fileSetNotifyParams_wrapper(wampcc::invocation_info info);
		void fileSetOverride_wrapper(wampcc::invocation_info info);
		void fileInfo_wrapper(wampcc::invocation_info info);

		/* Serial Port functions */
		void serialOpen_wrapper(wampcc::invocation_info info);
		void serialClose_wrapper(wampcc::invocation_info info);
		void serialInfo_wrapper(wampcc::invocation_info info);

};

} // namespace fabui

#endif /* GCODE_SERVICE_WAMP_WRAPPER_HPP */