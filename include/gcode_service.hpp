/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: gcode_service.hpp
 * @brief GCode WAMP service definition
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef GCODE_SERVICE_HPP
#define GCODE_SERVICE_HPP

#include <algorithm>
#include <functional>
#include <string>
#include <thread>
#include <memory>
#include <mutex>
#include <forward_list>

#include <serialxx/thread_safe.hpp>
#include <fabui/wamp/session.hpp>
#include <fabui/thread/queue.hpp>
#include <fabui/thread/event.hpp>

#include "plugins/gcode/base.hpp"
#include "gcode_file.hpp"
#include "gcode_parser.hpp"
#include "gcode_modifier.hpp"
#include "gcode_monitor.hpp"
#include "gcode_types.hpp"
#include "gcode_state.hpp"
#include "command.hpp"

#define ASYNC_START_ID  1

namespace fabui {

class GCodeService {
	public:
		/// Create a GCodeService
		GCodeService(
			const std::string& portName,
			unsigned baudRate=115200,
			unsigned timeout=1000);

		virtual ~GCodeService();

		/* Service control */

		/**
		 * Start service.
		 */
		bool start();
		/**
		 * Stop service.
		 */
		void stop();
		/**
		 * Terminate the service and abort all pending operations.
		 */
		void terminate();
		/**
		 * Suspend the service. If there are pending operations suspend will not happen.
		 *
		 * @param force If true pending operations will be aborted and the service will be suspended
		 * @returns true on success, false in case there are pending operations and force is false
		 */
		bool suspend(bool force=false);
		/**
		 * Resume a previously suspended service.
		 *
		 * @returns true on success, false on failure
		 */
		bool resume();

		//
		wampcc::json_object info();

		/* Gcode functions */

		/**
		 * Send a gcode command and wait for the reply or until timeout is reached. Wait is also aborted if the command is internally aborted
		 * due to service being suspended or terminated.
		 *
		 * @param code GCode command
		 * @param reply Expected reply for command finish detection
		 * @param use_regex If true use reply string as regex pattern
		 * @param timeout Allowed time in milliseconds before the command is aborted
		 *
		 * @returns GCode command with status and reply data.
		 */
		std::shared_ptr<command::Generic> send(const std::string &code, const std::string &reply = "ok", bool use_regex=false, unsigned timeout=1000);

		/**
		 * Send a gcode command and return immediately with an async Id.
		 *
		 * @param code GCode command
		 * @param reply Expected reply for command finish detection
		 * @param use_regex If true use reply string as regex pattern
		 * @param timeout Allowed time in milliseconds before the command is aborted
		 *
		 * @returns New command async Id
		 */
		unsigned sendAsync(const std::string &code, const std::string &reply = "ok", bool use_regex=false, unsigned timeout=1000);

		/**
		 * Abort a running async command.
		 *
		 * @param asyncId Command Id
		 * @returns true if command id is found and aborted, false otherwise
		 */
		bool abortAsync(unsigned asyncId);

		/* GCode File functions */

		/**
		 * Load a gcode file.
		 *
		 * @param fileName GCode filename
		 * @returns true on success, false otherwise
		 */
		bool fileLoad(const std::string& fileName);
		/**
		 * Start pushing commands from file to serial port.
		 *
		 * @returns true on success, false otherwise
		 */
		bool filePush();
		/**
		 * Load and start pushing commands from file to serial port.
		 *
		 * @param fileName GCode filename
		 * @returns true on success, false otherwise
		 */
		bool fileLoadAndPush(const std::string& fileName);
		/**
		 * Pause file pushing operation.
		 *
		 * @returns true on success, false otherwise
		 */
		bool filePause();
		/**
		 * Resume file pushing operation.
		 *
		 * @returns true on success, false otherwise
		 */
		bool fileResume();
		/**
		 * Abort file pushing operation.
		 *
		 * @returns true on success, false otherwise
		 */
		bool fileAbort();


		bool fileSetNotifyParams(float min_progress, bool status_update, bool height_update);

		bool fileSetOverride(const std::string& override, float value, unsigned index);

		wampcc::json_object fileInfo();

		/* Serial Port functions */
		bool serialOpen();
		bool serialClose(bool force=false);
		wampcc::json_object   serialInfo();

		void publish(const std::string& topic, wampcc::wamp_args args, wampcc::json_object options = {});

		void setWampSession(IWampSession* session);
		IWampSession* getWampSession();

		// std::future<wampcc::result_info> call(const std::string& procedure, wampcc::wamp_args args = {}, wampcc::json_object options = {});

		// std::function<void(
		// 	const std::string& topic,
		// 	wampcc::wamp_args args,
		// 	wampcc::json_object
		// 	)> publish_fn;

		// std::function<std::future<wampcc::result_info>(
		// 	const std::string& procedure,
		// 	wampcc::wamp_args args,
		// 	wampcc::json_object options
		// 	)> call_fn;
	protected:

		IWampSession *m_session;
		// unsigned tx_count;
		// unsigned rx_count;

		// plugins
		plugin::gcode::Plugin* loadGcodePlugin(const std::string& filename);
		std::unique_ptr<plugin::gcode::Plugin> m_gcode_engine;

		// serial
		unsigned m_timeout;
		serialxx::thread_safe::Serial m_serial;

		void writeLine(const std::string& line);
		std::shared_ptr<command::Generic> sendGcode(const std::string& gcode);
		bool sendGcode(std::shared_ptr<command::Generic> cmd, const std::string& comment = "");

		// queues
		Queue< std::shared_ptr<command::Generic> > cq;
		Queue< std::shared_ptr<command::Generic> > rq;
		struct Publication {
			std::string topic;
			wampcc::wamp_args args;
			wampcc::json_object options;
		};
		Queue<Publication,0> pq;

		// Async timeout
		std::mutex m_timeout_list_mut;
		std::forward_list< std::shared_ptr<command::Generic> > m_timeout_list;

		std::mutex m_active_mut;
		std::shared_ptr<command::Generic> m_active;
		std::mutex m_last_mut;
		std::shared_ptr<command::Generic> m_last;

		// threads
		Event e_tx_state_changed;
		Event e_rx_state_changed;
		Event e_resume;

		std::mutex m_running_mut;
		enum class State {
			STOPPED,
			RUNNING,
			SUSPENDED
		};
		State m_running_state;

		std::thread m_sender_thread;
		std::thread m_receiver_thread;
		std::thread m_publisher_thread;
		std::thread m_timeout_thread;

		void receiverThreadLoop();
		void senderThreadLoop();
		void timeoutThreadLoop();
		void publisherThreadLoop();

		// async
		std::mutex m_async_mut;
		unsigned m_async_id;

		// file
		std::mutex m_file_status_mut;
		FileStatus m_file_status;
		GCodeFile m_gfile;

		std::mutex m_file_notify_mut;
		float m_old_progress;
		FileNotifySettings m_file_notify_settings;

		void pushLines();
		void processReplyLine(const std::string& line);

		bool canAbort();
		bool abort(bool force=false);

		// gcode pre-processors
		GCodeParser m_parser;
		GCodeState  m_state;
		GCodeModifier m_modifier;
		GCodeMonitor  m_monitor;
};

} // namespace fabui

#endif /* GCODE_SERVICE_HPP */
